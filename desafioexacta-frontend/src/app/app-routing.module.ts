import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { GastoComponent } from './views/gasto/gasto.component';
import { GastoCreateComponent } from './components/gasto/gasto-create/gasto-create.component';
import { GastoDetailComponent } from './components/gasto/gasto-detail/gasto-detail.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "gasto",
    component: GastoComponent
  },
  {
    path: "gasto/create",
    component: GastoCreateComponent
  },
  {
    path: "gasto/detail/:id",
    component: GastoDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
