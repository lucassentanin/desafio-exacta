import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GastoService } from '../gasto.service';
import { Gasto } from '../gasto.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-gasto-create',
  templateUrl: './gasto-create.component.html',
  styleUrls: ['./gasto-create.component.css']
})
export class GastoCreateComponent implements OnInit {

  gasto: Gasto = {
    nomePessoa: '',
    descricao: '',
    dataHora: null,
    valor: null,
    tag: ''
  }

  form: FormGroup;

  MSG_CAMPO_OBRIGATORIO = 'Por favor, preencha o campo obrigatório.';

  constructor(private gastoService: GastoService,
              private router: Router,
              private builder: FormBuilder) {
  }

  ngOnInit(): void {
    this.criarFormulario()
  }

  criarFormulario() {
    this.form = this.builder.group({
      nomePessoa: [null, Validators.required],
      descricao: [null, Validators.required],
      dataHora: [null, Validators.required],
      valor: [null, Validators.required],
      tag: [null, Validators.required],
    });
  }

  criarGasto(): void {
    if(this.form.valid){
      this.gastoService.criarGasto(this.form.value).subscribe(() => {
        this.gastoService.showMessage('Gasto criado com sucesso!')
        this.router.navigate(['/gasto'])
      });
    }else{
      Object.keys(this.form.controls).forEach(campo => {
        const controle = this.form.get(campo)
        controle.markAllAsTouched()
      })
    }
  }

  cancelar(): void {
    this.router.navigate(['/gasto'])
  }

}