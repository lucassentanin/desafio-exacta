import { Component, OnInit } from '@angular/core';
import { Gasto } from '../gasto.model';
import { GastoService } from '../gasto.service';

@Component({
  selector: 'app-gasto-read',
  templateUrl: './gasto-read.component.html',
  styleUrls: ['./gasto-read.component.css']
})
export class GastoReadComponent implements OnInit {

  gastos: Gasto[]
  displayedColumns = ['nomePessoa', 'descricao', 'valor', 'acao']

  constructor(private gastoService: GastoService) { }

  ngOnInit(): void {
    this.gastoService.buscarGastos().subscribe(gastos => {
      this.gastos = gastos;
    })
  }

}
