import { Component, OnInit } from '@angular/core';
import { GastoReadComponent } from '../gasto-read/gasto-read.component';
import { GastoService } from '../gasto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Gasto } from '../gasto.model';

@Component({
  selector: 'app-gasto-detail',
  templateUrl: './gasto-detail.component.html',
  styleUrls: ['./gasto-detail.component.css']
})
export class GastoDetailComponent implements OnInit {

  gasto: Gasto = {
    nomePessoa: '',
    descricao: '',
    dataHora: null,
    valor: null,
    tag: ''
  }

  constructor(private gastoService: GastoService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.gastoService.buscarGastoPorCodigo(id).subscribe(gasto => {
      this.gasto = gasto
    })
  }

  cancelar(): void {
    this.router.navigate(['/gasto']);
  }

}
