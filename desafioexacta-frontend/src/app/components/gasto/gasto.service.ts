import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Gasto } from './gasto.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GastoService {

  baseUrl = '/api/gastos';

  constructor(private snackBar: MatSnackBar,
              private http: HttpClient) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, '', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  criarGasto(gasto: Gasto): Observable<Gasto> {
    const url = `${this.baseUrl}/salvar`
    return this.http.post<Gasto>(url, gasto)
  }

  buscarGastos(): Observable<Gasto[]>{
    const url = `${this.baseUrl}/buscar-todos`
    return this.http.get<Gasto[]>(url)
  }

  buscarGastoPorCodigo(id: string): Observable<Gasto> {
    const url = `${this.baseUrl}/buscar-gasto/${id}`
    return this.http.get<Gasto>(url)
  }
}
