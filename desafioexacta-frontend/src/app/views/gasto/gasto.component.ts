import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gasto',
  templateUrl: './gasto.component.html',
  styleUrls: ['./gasto.component.css']
})
export class GastoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navegarCriacaoGasto(): void {
    this.router.navigate(['/gasto/create'])
  }
}
