# Desafio Exactaworks #

Sistema de controle de gastos.


### Tecnologias ###


* Java 11
* Spring Data JPA/Hibernate
* Spring Boot
* Spring Cloud
* Angular 9
* Angular Material
* Database SQL Server


### Desenvolvido por ###


* Lucas Montrezor Sentanin
* lucas_ms06@hotmail.com