package br.com.desafio.gastoservice.service;

import br.com.desafio.gastoservice.model.Gasto;

public interface GastoService {

	Iterable<Gasto> buscarTodos();

	Gasto buscarGastoPorId(Long id);

	void salvarGasto(Gasto gasto);

}
