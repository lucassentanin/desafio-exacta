package br.com.desafio.gastoservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.gastoservice.model.Gasto;
import br.com.desafio.gastoservice.repository.GastoRepository;
import br.com.desafio.gastoservice.service.GastoService;

@Service
public class GastoServiceImpl implements GastoService {

	@Autowired
	private GastoRepository repository;

	@Override
	public Iterable<Gasto> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public Gasto buscarGastoPorId(Long id) {
		return repository.findById(id).orElseGet(Gasto::new);
	}

	@Override
	public void salvarGasto(Gasto gasto) {
		repository.save(gasto);
	}

}
