package br.com.desafio.gastoservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.desafio.gastoservice.model.Gasto;

public interface GastoRepository extends JpaRepository<Gasto, Long> {

}
