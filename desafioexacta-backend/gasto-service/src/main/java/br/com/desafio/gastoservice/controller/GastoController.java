package br.com.desafio.gastoservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.gastoservice.model.Gasto;
import br.com.desafio.gastoservice.service.GastoService;

@RestController
@RequestMapping("/gastos")
public class GastoController {

	@Autowired
	private GastoService service;

	@GetMapping(path = "/buscar-todos")
	public Iterable<Gasto> buscarTodos() {
		return service.buscarTodos();
	}

	@GetMapping(path = "/buscar-gasto/{id}")
	public Gasto buscarGastoPorId(@PathVariable("id") Long id) {
		return service.buscarGastoPorId(id);
	}

	@PostMapping(path = "/salvar")
	public void salvarGasto(@RequestBody Gasto gasto) {
		service.salvarGasto(gasto);
	}

}
