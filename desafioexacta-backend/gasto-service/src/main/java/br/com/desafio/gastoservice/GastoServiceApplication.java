package br.com.desafio.gastoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class GastoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GastoServiceApplication.class, args);
	}

}
