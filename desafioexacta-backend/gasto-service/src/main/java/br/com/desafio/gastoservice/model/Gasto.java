package br.com.desafio.gastoservice.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@SequenceGenerator(name = "SEQ_TB_GASTO", sequenceName = "SEQ_TB_GASTO", initialValue = 1, allocationSize = 1)
@Table(name = "TB_GASTO")
public class Gasto implements Serializable {

	private static final long serialVersionUID = 6586419803975421580L;

	@Id
	@GeneratedValue(generator = "SEQ_TB_GASTO", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_GASTO")
	private Long id;

	@Column(name = "NOME_PESSOA")
	private String nomePessoa;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "DATA_HORA")
	private LocalDateTime dataHora;

	@Column(name = "VALOR")
	private BigDecimal valor;

	@Column(name = "TAG")
	private String tag;

}
